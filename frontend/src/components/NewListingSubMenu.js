import React, {useState} from 'react';
import {Drawer, Toolbar, Button, Grid, List, ListItem} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ListItemButton from '@mui/material/ListItemButton';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    width: '100%',
    height: '100%',
  },
  listItem: {
    padding: '10',
    textAlign: 'left',
    paddingRight: '240',
  },
  error: {
    display: 'hidden',
    color: 'white',
  },
}));
/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function NewListingSubMenu(props) {
  const classes = useStyles();
  const [subCat, setSubCat] = useState([]);
  const [displaySubMenu, setDisplaySubMenu] = useState(false);
  const conditions = ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'];
  const close = () => {
    setSubCat([]);
    props.setOpenNewListingSubMenu(false);
  };
  const catClick = (cat) => {
    props.setSelectedCat(cat.id);
    props.setnewListingSubMenuType(cat.category.name);
    fetch(`http://localhost:3010/v0/categories?subcategories=${cat.id}`,
      {
        method: 'GET',
      })
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((json) => {
        setSubCat(json);
        setDisplaySubMenu(true);
      })
      .catch((err) => {
        setSubCat('Error');
      });
  };
  const updateListing = (value) => {
    if (props.newListingSubMenuType === 'condition') {
      const listing = props.listing;
      props.listing['condition'] = value;
      props.setListing(listing);
    } else {
      const listing = props.listing;
      props.listing['subcategory'] = value;
      props.setListing(listing);
    }
    close();
  };
  const renderSubMenu = () => {
    if (props.newListingSubMenuType === 'category') {
      return (
        <List>
          {props.categories.map((cat) => (
            <ListItem key={`cat-${cat.id}`}>
              <ListItemButton key={cat.category.name}
                onClick={() => catClick(cat)}
                className={classes.listItem}>
                {cat.category.name}
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      );
    } else if (props.newListingSubMenuType === 'condition') {
      return (
        <List>
          {conditions.map((con) => (
            <ListItem key={`sub-${con}`}>
              <ListItemButton key={con}
                onClick = {() => updateListing(con)}
                className={classes.listItem}>
                {con}
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      );
    }
  };
  return (
    <Drawer open={props.openNewListingSubMenu} anchor={'bottom'}
      variant='persistent'
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Toolbar>
        <Grid justifyContent='space-between'
          container
          spacing={0}>
          <p key={'0'}style={{color: 'black',
            fontWeight: 'bold'}}> {props.newListingSubMenuType} </p>
          <Button aria-label='close-submenu'
            style={{marginTop: 8, backgroundColor: '#dadada',
              borderRadius: 50, overflow: 'hidden',
              height: 35, width: 35}}
            onClick = {() => close()}>
            <CloseIcon />
          </Button>
        </Grid>
      </Toolbar>
      {renderSubMenu()}
      {displaySubMenu &&
        <List>
          {subCat.map((cat) => (
            <ListItem key={`subcat-${cat.id}`}>
              <ListItemButton key={cat.category.name}
                onClick = {() => updateListing(cat.id)}
                className={classes.listItem}>
                {cat.category.name}
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      }
      {subCat === 'Error' &&
        <div>Error</div>
      }
    </Drawer>
  );
}

export default NewListingSubMenu;
