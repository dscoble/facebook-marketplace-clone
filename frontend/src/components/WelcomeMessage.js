import React from 'react';
import {Button} from '@material-ui/core';
/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function WelcomeMessage(props) {
  return (
    <div style={{marginLeft: -10, padding: 10,
      marginTop: 50, width: '100%', backgroundColor: '#d59899'}}>
      <p style={{fontWeight: 'bold'}}>
        Buy and sell {props.selectedCat.category.name.toLowerCase()} locally
        or have something new shipped from stores.
      </p>
      <p style={{}}>
        Log in to get the full Facebook Marketplace experience.
      </p>
      <Button onClick={() => props.setOpenLogin(true)}
        style={{marginRight: 10, backgroundColor: 'white'}}>
        Log In
      </Button>
      <Button style={{flex: 1, backgroundColor: 'white', width: '60%'}}>
        Learn More
      </Button>
    </div>
  );
}
export default WelcomeMessage;
