import React, {useState} from 'react';
// import {makeStyles} from '@material-ui/core/styles';

// import CloseIcon from '@material-ui/icons/Close';

// const useStyles = makeStyles((theme) => ({
//   drawerPaper: {
//     width: '100%',
//     height: '100%',
//   },
// }));
/**
 * Simple component with no state.
 * @param {object} props
 * @return {object} JSX
 */
function LoginFast(props) {
//   const classes = useStyles();
  const [user, setUser] = useState({email: '', password: ''});
  const [error, setError] = useState('');

  const handleInputChange = (event) => {
    // console.log('change');
    const {value, name} = event.target;
    const u = user;
    u[name] = value;
    // console.log(value, name);
    // console.log(user);
    setUser(u);
    // console.log(user);
  };
  // console.log(user);
  const onSubmit = (event) => {
    // console.log(user);
    event.preventDefault();
    fetch('http://localhost:3010/v0/authenticate', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        if (!res.ok) {
          throw res;
        }
        // console.log(res.json);
        return res.json();
      })
      .then((json) => {
        localStorage.setItem('user', JSON.stringify(json));
        props.setLoggedIn(true);
        props.setOpenLogin(false);
        setError('');
      })
      .catch((err) => {
        console.log(err);
        setError('Error logging in, please try again');
      });
  };
  // console.log(user);
  return (
    <form onSubmit={onSubmit} style={{
      display: 'inline-block',
    }}>
      <input style={{
        width: '150px', height: '20px', borderRadius: '6px',
        margin: 10, padding: 2,
      }}
      aria-label="email"
      type="email"
      name="email"
      placeholder="Email"
      onChange={handleInputChange}
      required />
      <input style={{
        width: '150px', height: '20px', borderRadius: '6px',
        margin: 10, padding: 2,
      }}
      aria-label="password"
      type="password"
      name="password"
      placeholder="Password"
      onChange={handleInputChange}
      required />
      <input style={{
        width: '50px', height: '30px', borderRadius: '6px',
        backgroundColor: 'blue', color: 'white',
        padding: 2,
      }} aria-label="log in" type="submit" value="Log In"/>
      {error &&
            <div style={{color: 'red'}}>{error}</div>
      }
    </form>
  );
}

export default LoginFast;
