import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen, waitFor} from '@testing-library/react';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import NewListingSubMenu from '../NewListingSubMenu';

const URL = 'http://localhost:3010/v0/categories';
const mockSubCat = [
  {
    'id': 'c5f2f682-79c1-4c12-98e4-7db79f0df9ca',
    'category': {
      'name': 'Motorcycles',
      'filters': [
        {
          'condition': ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'],
        },
      ],
      'parent': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
    },
  },
];
const mockCat = [
  {
    'id': 'c5f2f682-79c1-4c12-98e4-7db79f0df9c7',
    'category': {
      'name': 'Vehicles',
      'filters': [
        {
          'condition': ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'],
        },
      ],
    },
  },
];
const mockSetnewListingSubMenuType = jest.fn();
const mockSetListing = jest.fn();
const mockSetOpenNewListingSubMenu = jest.fn();
const mockSetSelectedCat = jest.fn();

const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    return res(ctx.json(mockSubCat));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Render Category Submenu', async () => {
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'category'}
    categories={mockCat}
  />);
});

test('Close Submenu', async () => {
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'category'}
    categories={mockCat}
    setOpenNewListingSubMenu={mockSetOpenNewListingSubMenu}
  />);
  const close = screen.getByLabelText('close-submenu');
  fireEvent.click(close);
  expect(mockSetOpenNewListingSubMenu).toBeCalled();
});

test('Render Condition Submenu', async () => {
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'condition'}
    categories={mockCat}
  />);
});

test('Render Condition invalid Submenu', async () => {
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'dsfdsf'}
    categories={mockCat}
  />);
});

test('Click Category Submenu', async () => {
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'category'}
    categories={mockCat}
    listing={{}}
    setnewListingSubMenuType={mockSetnewListingSubMenuType}
    setListing={mockSetListing}
    setSelectedCat = {mockSetSelectedCat}
  />);
  const button = screen.getByText(mockCat[0].category.name);
  fireEvent.click(button);
  await waitFor(() =>
    screen.getByText(mockSubCat[0].category.name));
  expect(mockSetnewListingSubMenuType).toBeCalled();
});

test('Click Category Submenu then click sub category', async () => {
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'category'}
    categories={mockCat}
    listing={{}}
    setnewListingSubMenuType={mockSetnewListingSubMenuType}
    setListing={mockSetListing}
    setOpenNewListingSubMenu={mockSetOpenNewListingSubMenu}
    setSelectedCat = {mockSetSelectedCat}
  />);
  const button = screen.getByText(mockCat[0].category.name);
  fireEvent.click(button);
  const subcat = await waitFor(() =>
    screen.getByText(mockSubCat[0].category.name));
  fireEvent.click(subcat);
  expect(mockSetListing).toBeCalled();
  expect(mockSetnewListingSubMenuType).toBeCalled();
  expect(mockSetOpenNewListingSubMenu).toBeCalled();
});

test('Render and Click Condition Submenu', async () => {
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'condition'}
    categories={mockCat}
    listing={{}}
    setnewListingSubMenuType={mockSetnewListingSubMenuType}
    setListing={mockSetListing}
    setOpenNewListingSubMenu={mockSetOpenNewListingSubMenu}
  />);
  const button = screen.getByText('New');
  fireEvent.click(button);
  expect(mockSetListing).toBeCalled();
  expect(mockSetOpenNewListingSubMenu).toBeCalled();
});

test('Click Category Submenu with server error', async () => {
  server.use(
    rest.get(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<NewListingSubMenu
    openNewListingSubMenu={true}
    newListingSubMenuType={'category'}
    categories={mockCat}
    listing={{}}
    setnewListingSubMenuType={mockSetnewListingSubMenuType}
    setListing={mockSetListing}
    setSelectedCat = {mockSetSelectedCat}
  />);
  const button = screen.getByText(mockCat[0].category.name);
  fireEvent.click(button);
  await waitFor(() =>
    screen.getByText('Error'));
});
