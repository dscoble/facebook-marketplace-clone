import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen, waitFor} from '@testing-library/react';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import ListingsPopUp from '../ListingPopUp';

const URL = 'http://localhost:3010/v0/responses';
const getListing = 'http://localhost:3010/v0/listings/:id';
const mockResponse = {
  'id': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
  'listing': '3fa85f64-5717-4562-b3fc-2c963f66afa6',
  'response': {
    'firstname': 'string',
    'lastname': 'string',
    'text': 'string',
  },
};
const mockActiveListing = {
  'id': '4944550c-9fec-4bb4-8dfd-1e5b411fa0b9',
  'listing': {
    'description': 'this is a car',
    'price': '$17',
    'title': 'a car',
    'location': 'santa cruz, ca',
    'images': ['https://assets-global.website-files.com/6047b5ff5b990938b601662f/60a520695b360d1ac382fbcc_cu-2-p-1080.jpeg'],
    'condition': 'New',
  },
  'member': 'fbec17cc-9d39-4bff-be43-afc60e5f80db',
  'category': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
};
const mockListing = {
  'id': '4944550c-9fec-4bb4-8dfd-1e5b411fa0b9',
  'listing': {
    'description': 'this is a car',
    'price': '$17',
    'title': 'a not a car',
    'location': 'santa cruz, ca',
    'images': ['https://assets-global.website-files.com/6047b5ff5b990938b601662f/60a520695b360d1ac382fbcc_cu-2-p-1080.jpeg'],
    'condition': 'New',
  },
  'member': 'fbec17cc-9d39-4bff-be43-afc60e5f80db',
  'category': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
};
const user = {
  'firstname': 'test',
  'lastname': 'test',
  'email': 'testtest@test.com',
  'accessToken': 'dummy',
};

localStorage.setItem('user', JSON.stringify(user));

const mockHandleOpenListing = jest.fn();

const server = setupServer(
  rest.post(URL, (req, res, ctx) => {
    return res(ctx.json(mockResponse));
  }),
  rest.get(getListing, (req, res, ctx) => {
    return res(ctx.json(mockListing));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Render Popup', async () => {
  render(<ListingsPopUp
    openListing={true}
    activeListing={mockActiveListing}
    loggedIn={true}
  />);
  await waitFor(() =>
    screen.getByText(mockListing.listing.title));
});

test('Render Popup with no active listing', async () => {
  render(<ListingsPopUp
    openListing={true}
    activeListing={null}
    loggedIn={true}
  />);
});

test('Render Popup with no open listing', async () => {
  render(<ListingsPopUp
    openListing={false}
    activeListing={mockActiveListing}
    loggedIn={true}
  />);
  const listing = screen.queryByText(mockListing.listing.title);
  expect(listing).toBeNull();
});

test('Close Popup', async () => {
  render(<ListingsPopUp
    openListing={true}
    activeListing={mockActiveListing}
    handleOpenListing={mockHandleOpenListing}
    loggedIn={true}
  />);
  const button = screen.getByLabelText('close-listing');
  fireEvent.click(button);
  expect(mockHandleOpenListing).toBeCalled();
});

test('Response with Enter', async () => {
  render(<ListingsPopUp
    openListing={true}
    activeListing={mockActiveListing}
    handleOpenListing={mockHandleOpenListing}
    loggedIn={true}
  />);
  await waitFor(() =>
    screen.getByText(mockListing.listing.title));
  const input = screen.getByLabelText('response');
  fireEvent.change(input, {target: {value: 'hello'}});
  fireEvent.keyDown(input, {key: 'Enter'});
  await waitFor(() => expect(mockHandleOpenListing).toBeCalled());
});

test('Response with Random Key', async () => {
  render(<ListingsPopUp
    openListing={true}
    activeListing={mockActiveListing}
    handleOpenListing={mockHandleOpenListing}
    loggedIn={true}
  />);
  await waitFor(() =>
    screen.getByText(mockListing.listing.title));
  const input = screen.getByLabelText('response');
  fireEvent.change(input, {target: {value: 'hello'}});
  fireEvent.keyDown(input, {key: 'G'});
});

test('Server Error for listing', async () => {
  server.use(
    rest.get(getListing, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<ListingsPopUp
    openListing={true}
    activeListing={mockActiveListing}
    handleOpenListing={mockHandleOpenListing}
    loggedIn={true}
  />);
  await waitFor(() => screen.getByText('Failed to get listing'));
});

test('Server Error', async () => {
  server.use(
    rest.post(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<ListingsPopUp
    openListing={true}
    activeListing={mockActiveListing}
    handleOpenListing={mockHandleOpenListing}
    loggedIn={true}
  />);
  await waitFor(() =>
    screen.getByText(mockListing.listing.title));
  const input = screen.getByLabelText('response');
  fireEvent.change(input, {target: {value: 'hello'}});
  fireEvent.keyDown(input, {key: 'Enter'});
  await waitFor(() => screen.getByText('Failed to Send'));
});

test('Not Logged In', async () => {
  render(<ListingsPopUp
    openListing={true}
    activeListing={mockActiveListing}
    handleOpenListing={mockHandleOpenListing}
    loggedIn={false}
  />);
  await waitFor(() => screen.getByText('Log in to Respond'));
});
