import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import {screen, waitFor} from '@testing-library/react';

import Listings from '../Listings';

const URL = 'http://localhost:3010/v0/listings';
const search = `http://localhost:3010/v0/search`;
const cat = `http://localhost:3010/v0/categories/:id`;
const mockListings = [
  {
    'id': '4944550c-9fec-4bb4-8dfd-1e5b411fa0b9',
    'listing': {
      'description': 'this is a car',
      'price': '$17',
      'title': 'a car',
      'location': 'santa cruz, ca',
      'images': ['https://assets-global.website-files.com/6047b5ff5b990938b601662f/60a520695b360d1ac382fbcc_cu-2-p-1080.jpeg'],
      'condition': 'New',
    },
    'member': 'fbec17cc-9d39-4bff-be43-afc60e5f80db',
    'category': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
  },
  {
    'id': '3b963727-818e-4272-9245-39dade137108',
    'listing': {
      'description': 'this is a shirt',
      'price': '$200',
      'title': 'a shirt',
      'location': 'santa cruz, ca',
      'images': ['https://lp2.hm.com/hmgoepprod?set=quality%5B79%5D%2Csource%5B%2F1e%2F86%2F1e86c77fb86afc19daad9acb5b39470e7bc5ca1f.jpg%5D%2Corigin%5Bdam%5D%2Ccategory%5Bmen_tshirtstanks_shortsleeve%5D%2Ctype%5BDESCRIPTIVESTILLLIFE%5D%2Cres%5Bm%5D%2Chmver%5B2%5D&call=url[file:/product/main]', 'https://media.dior.com/couture/ecommerce/media/catalog/product/9/e/1594849779_043J615A0589_C980_E08_GHC.jpg?imwidth=800'],
      'condition': 'Used - Fair',
    },
    'member': 'a829dc0c-0e0b-4514-bf8f-47ef5864a0e0',
    'category': 'f991d7c7-9db1-409f-a052-fac469e612ab',
  },
];

const searchMockListings = [
  {
    'id': '4944550c-9fec-4bb4-8dfd-1e5b411fa0b0',
    'listing': {
      'description': 'this is a car',
      'price': '$17',
      'title': 'a car',
      'location': 'santa cruz, ca',
      'images': ['https://assets-global.website-files.com/6047b5ff5b990938b601662f/60a520695b360d1ac382fbcc_cu-2-p-1080.jpeg'],
      'condition': 'New',
    },
    'member': 'fbec17cc-9d39-4bff-be43-afc60e5f80db',
    'category': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
  },
  {
    'id': '3b963727-818e-4272-9245-39dade137103',
    'listing': {
      'description': 'this is a shirt',
      'price': '$200',
      'title': 'a shirt',
      'location': 'santa cruz, ca',
      'images': ['https://lp2.hm.com/hmgoepprod?set=quality%5B79%5D%2Csource%5B%2F1e%2F86%2F1e86c77fb86afc19daad9acb5b39470e7bc5ca1f.jpg%5D%2Corigin%5Bdam%5D%2Ccategory%5Bmen_tshirtstanks_shortsleeve%5D%2Ctype%5BDESCRIPTIVESTILLLIFE%5D%2Cres%5Bm%5D%2Chmver%5B2%5D&call=url[file:/product/main]', 'https://media.dior.com/couture/ecommerce/media/catalog/product/9/e/1594849779_043J615A0589_C980_E08_GHC.jpg?imwidth=800'],
      'condition': 'Used - Fair',
    },
    'member': 'a829dc0c-0e0b-4514-bf8f-47ef5864a0e0',
    'category': 'f991d7c7-9db1-409f-a052-fac469e612ab',
  },
];
const mockCategoryListing = [
  {
    'id': '4944550c-9fec-4bb4-8dfd-1e5b411fa0b1',
    'listing': {
      'description': 'this is a car',
      'price': '$17',
      'title': 'a car',
      'location': 'santa cruz, ca',
      'images': ['https://assets-global.website-files.com/6047b5ff5b990938b601662f/60a520695b360d1ac382fbcc_cu-2-p-1080.jpeg'],
      'condition': 'New',
    },
    'member': 'fbec17cc-9d39-4bff-be43-afc60e5f80db',
    'category': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
  },
  {
    'id': '3b963727-818e-4272-9245-39dade137102',
    'listing': {
      'description': 'this is a shirt',
      'price': '$200',
      'title': 'a shirt',
      'location': 'santa cruz, ca',
      'images': ['https://lp2.hm.com/hmgoepprod?set=quality%5B79%5D%2Csource%5B%2F1e%2F86%2F1e86c77fb86afc19daad9acb5b39470e7bc5ca1f.jpg%5D%2Corigin%5Bdam%5D%2Ccategory%5Bmen_tshirtstanks_shortsleeve%5D%2Ctype%5BDESCRIPTIVESTILLLIFE%5D%2Cres%5Bm%5D%2Chmver%5B2%5D&call=url[file:/product/main]', 'https://media.dior.com/couture/ecommerce/media/catalog/product/9/e/1594849779_043J615A0589_C980_E08_GHC.jpg?imwidth=800'],
      'condition': 'Used - Fair',
    },
    'member': 'a829dc0c-0e0b-4514-bf8f-47ef5864a0e0',
    'category': 'f991d7c7-9db1-409f-a052-fac469e612ab',
  },
];

const mockHandleActiveListing = jest.fn();
const mockHandleOpenListing = jest.fn();

const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    const subcat = req.url.searchParams.get('subcategory');
    if (subcat) {
      return res(ctx.json(mockCategoryListing));
    } else {
      return res(ctx.json(mockListings));
    }
  }),
  rest.get(cat, (req, res, ctx) => {
    return res(ctx.json(mockCategoryListing));
  }),
  rest.get(search, (req, res, ctx) => {
    return res(ctx.json(searchMockListings));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

/**
 */
test('Render Listings', async () => {
  render(<Listings />);
  for (let i = 0; i < mockListings.length; i++) {
    await waitFor(() =>
      screen.getByLabelText(`listing-${mockListings[i]['id']}`));
  };
});

test('Click Listing', async () => {
  render(
    <Listings
      handleActiveListing={mockHandleActiveListing}
      handleOpenListing={mockHandleOpenListing}
    />);
  for (let i = 0; i < mockListings.length; i++) {
    await waitFor(() =>
      screen.getByLabelText(`listing-${mockListings[i]['id']}`));
    fireEvent.click(screen.getByText(mockListings[i].listing.title));
    expect(mockHandleActiveListing).toBeCalled();
    expect(mockHandleOpenListing).toBeCalled();
  }
});

test('Server Error for listings', async () => {
  server.use(
    rest.get(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<Listings />);
  await waitFor(() => screen.getByText('no listings'));
});

test('Render Search', async () => {
  render(<Listings search={'car'}/>);
  for (let i = 0; i < searchMockListings.length; i++) {
    await waitFor(() =>
      screen.getByLabelText(`listing-${searchMockListings[i]['id']}`));
  };
});

test('Server Error for search', async () => {
  server.use(
    rest.get(search, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<Listings search={'aefaef'}/>);
  await waitFor(() => screen.getByText('no listings'));
});

test('Render Subcat', async () => {
  render(<Listings selectedSubCat={'c7c385c1-d889-40e4-bd5c-a5d2806700b5'}/>);
  for (let i = 0; i < mockCategoryListing.length; i++) {
    await waitFor(() =>
      screen.getByLabelText(`listing-${mockCategoryListing[i]['id']}`));
  };
});

test('Server Error for subcat', async () => {
  server.use(
    rest.get(URL, (req, res, ctx) => {
      const subcat = req.url.searchParams.get('subcategory');
      if (subcat) {
        return res(ctx.status(500));
      }
    }),
  );
  render(<Listings selectedSubCat={'c7c385c1-d889-40e4-bd5c-a5d2806700b5'}/>);
  await waitFor(() => screen.getByText('no listings'));
});

test('Render Cat', async () => {
  render(<Listings selectedCat={'c7c385c1-d889-40e4-bd5c-a5d2806700b5'}/>);
  for (let i = 0; i < mockCategoryListing.length; i++) {
    await waitFor(() =>
      screen.getByLabelText(`listing-${mockCategoryListing[i]['id']}`));
  };
});

test('Server Error for cat', async () => {
  server.use(
    rest.get(cat, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<Listings selectedCat={'c7c385c1-d889-40e4-bd5c-a5d2806700b5'}/>);
  await waitFor(() => screen.getByText('no listings'));
});
