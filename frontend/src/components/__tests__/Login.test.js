import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen, waitFor} from '@testing-library/react';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import Login from '../Login';

const URL = 'http://localhost:3010/v0/authenticate';
const mockUser = {
  'id': 'id',
  'accessToken': 'dummy',
  'firstname': 'firstname',
  'lastname': 'lastname',
};

const mockSetLoggedIn = jest.fn();
const mockSetOpenLogin = jest.fn();
const mockSetOpenSignup = jest.fn();

const server = setupServer(
  rest.post(URL, (req, res, ctx) => {
    return res(ctx.json(mockUser));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

/**
 */
test('Login Renders', async () => {
  render(
    <Login
      openLogin={true}
    />);
});

test('Create Account', async () => {
  // Render login component
  render(
    <Login
      openLogin={true}
      setOpenSignup={mockSetOpenSignup}
    />);

  // Click create account button
  const signup = screen.getByText('Create An Account');
  fireEvent.click(signup);

  // Check that open signup function was called
  expect(mockSetOpenSignup).toBeCalled();
});

test('Login', async () => {
  // render login component
  render(
    <Login
      openLogin={true}
      setLoggedIn={mockSetLoggedIn}
      setOpenLogin={mockSetOpenLogin}
    />);

  // Enter values into input fields
  const email = screen.getByLabelText('email');
  fireEvent.change(email, {target: {value: 'email'}});
  const pass = screen.getByLabelText('password');
  fireEvent.change(pass, {target: {value: 'pass'}});

  // Submit the form
  const submit = screen.getByLabelText('submit');
  fireEvent.click(submit);

  // Wait for mocked function to be called
  await waitFor(() => expect(mockSetLoggedIn).toHaveBeenCalledTimes(1));
});

test('Login Server error', async () => {
  // mock server error
  server.use(
    rest.post(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );

  // render the login component
  render(
    <Login
      openLogin={true}
      setLoggedIn={mockSetLoggedIn}
      setOpenLogin={mockSetOpenLogin}
    />);

  // Enter values into input fields
  const email = screen.getByLabelText('email');
  fireEvent.change(email, {target: {value: 'email'}});
  const pass = screen.getByLabelText('password');
  fireEvent.change(pass, {target: {value: 'pass'}});

  // submit the form
  const submit = screen.getByLabelText('submit');
  fireEvent.click(submit);

  // wait for the error message to appear
  await waitFor(() => screen.getByText('Error logging in, please try again'));
});
