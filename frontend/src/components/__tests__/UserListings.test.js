import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen, waitFor} from '@testing-library/react';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import UserListings from '../UserListings';

const URL = 'http://localhost:3010/v0/listings/user/:id';

const mockListings = [
  {
    'id': '4944550c-9fec-4bb4-8dfd-1e5b411fa0b9',
    'listing': {
      'description': 'this is a car',
      'price': '$17',
      'title': 'a car',
      'location': 'santa cruz, ca',
      'images': ['https://assets-global.website-files.com/6047b5ff5b990938b601662f/60a520695b360d1ac382fbcc_cu-2-p-1080.jpeg'],
      'condition': 'New',
    },
    'member': 'fbec17cc-9d39-4bff-be43-afc60e5f80db',
    'category': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
  },
];
const user = {
  'firstname': 'test',
  'lastname': 'test',
  'email': 'testtest@test.com',
  'accessToken': 'dummy',
};
const mockSetUserDrawer = jest.fn();
localStorage.setItem('user', JSON.stringify(user));
const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    return res(ctx.json(mockListings));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Render Listings Logged in', async () => {
  render(<UserListings
    userDrawer={true}
    loggedIn={true}
  />);
  await waitFor(() =>
    screen.getByText(mockListings[0].listing.title));
});

test('Render Listings not Logged In', async () => {
  render(<UserListings
    userDrawer={true}
    loggedIn={false}
  />);
  await waitFor(() =>
    screen.getByText('Please Log In'));
});

test('Close Popup', async () => {
  render(<UserListings
    userDrawer={true}
    loggedIn={true}
    setUserDrawer={mockSetUserDrawer}
  />);
  const button = screen.getByLabelText('close-user-drawer');
  fireEvent.click(button);
  expect(mockSetUserDrawer).toBeCalled();
});

test('Server Error for listing', async () => {
  server.use(
    rest.get(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(<UserListings
    userDrawer={true}
    loggedIn={true}
  />);
  await waitFor(() =>
    screen.getByText('Failed to fetch'));
});
