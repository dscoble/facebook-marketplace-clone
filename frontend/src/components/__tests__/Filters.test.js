import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen} from '@testing-library/react';
import Filters from '../Filters';

const mockSetFilterDrawer = jest.fn();
test('Filter closes success', async () => {
  render(<Filters filterDrawer = {true}
    setFilterDrawer = {mockSetFilterDrawer}/>);
  fireEvent.click(screen.getAllByRole('closefilter')[0]);
  expect(mockSetFilterDrawer).toBeCalled();
});
