import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen} from '@testing-library/react';
import WelcomeMessage from '../WelcomeMessage';

const mockCategories = {
  'id': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
  'category': {
    'name': 'Vehicles',
    'filters': [
      {
        'condition': ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'],
        'make': ['Acura', 'BMW', 'Toyota', 'Volvo'],
        'color': ['Black', 'Grey', 'Red', 'Yellow',
          'Green', 'Blue', 'Purple', 'White', 'Brown', 'Pink'],
        'body': ['Covertible', 'Sedan', 'SUV', 'Truck'],
      },
    ],
  },
};
const mockSetOpenLogin = jest.fn();

test('Welcome Message Renders', async () => {
  render(<WelcomeMessage selectedCat = {mockCategories}/>);
});

test('Click login', async () => {
  render(<WelcomeMessage
    selectedCat = {mockCategories}
    setOpenLogin = {mockSetOpenLogin}
  />);
  const login = screen.getByText('Log In');
  fireEvent.click(login);
  expect(mockSetOpenLogin).toBeCalled();
});
