import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import {screen, waitFor} from '@testing-library/react';
import CategoryChooser from '../CategoryChooser';

const URL = 'http://localhost:3010/v0/categories';
const mockCategories = [
  {
    'id': 'c5f2f682-79c1-4c12-98e4-7db79f0df9c7',
    'category': {
      'name': 'Vehicles',
      'filters': [
        {
          'condition': ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'],
        },
      ],
    },
  },
];
const mockSetCategories = jest.fn();
const mockSetSelectedCat = jest.fn();
const mockToggleDrawer = jest.fn();
const mockSetInCat = jest.fn();
const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    return res(ctx.json(mockCategories));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Category Chooser Renders', async () => {
  render(
    <CategoryChooser
      categories = {[]}
      setCategories = {mockSetCategories}
    />);
  await waitFor(() =>
    expect(mockSetCategories).toBeCalled());
});

test('Server Error', async () => {
  server.use(
    rest.get(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(
    <CategoryChooser
      categories = {[]}
      setCategories = {mockSetCategories}
    />);
  await waitFor(() =>
    screen.getByText('Error fetching categories'));
});

test('Trigger Close Category Drawer', async () => {
  render(
    <CategoryChooser
      categories = {mockCategories}
      toggleDrawer = {mockToggleDrawer}
    />);
  const button = screen.getByLabelText('close-cat-drawer');
  fireEvent.click(button);
  expect(mockToggleDrawer).toBeCalled();
});

test('Pick Category', async () => {
  render(
    <CategoryChooser
      categories = {mockCategories}
      toggleDrawer = {mockToggleDrawer}
      setInCat = {mockSetInCat}
      setSelectedCat = {mockSetSelectedCat}
    />);
  const button = screen.getByText(mockCategories[0].category.name);
  fireEvent.click(button);
  expect(mockToggleDrawer).toBeCalled();
  expect(mockSetInCat).toBeCalled();
  expect(mockSetSelectedCat).toBeCalled();
});
