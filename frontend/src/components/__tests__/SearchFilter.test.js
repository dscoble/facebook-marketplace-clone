import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen} from '@testing-library/react';
import SearchFilter from '../SearchFilter';

const mockSetDrawer = jest.fn();
const mockSetFilterDrawer = jest.fn();
const mockSetSearch = jest.fn();
const mockSetOpenNewListing = jest.fn();
const mockSetUserDrawer = jest.fn();

test('Search Renders', async () => {
  render(<SearchFilter />);
});

test('Toggle Cat Drawer', async () => {
  render(
    <SearchFilter
      toggleDrawer={mockSetDrawer}
    />);
  fireEvent.click(screen.getByText('All Categories'));
  expect(mockSetDrawer).toBeCalled();
});

test('Toggle User Drawer', async () => {
  render(
    <SearchFilter
      setUserDrawer={mockSetUserDrawer}
    />);
  fireEvent.click(screen.getByLabelText('user'));
  expect(mockSetUserDrawer).toBeCalled();
});

test('Toggle Filter Drawer', async () => {
  render(
    <SearchFilter
      setFilterDrawer={mockSetFilterDrawer}
    />);
  fireEvent.click(screen.getByText('Filters'));
  expect(mockSetFilterDrawer).toBeCalled();
});

test('Toggle Sell Drawer', async () => {
  render(
    <SearchFilter
      setOpenNewListing={mockSetOpenNewListing}
    />);
  fireEvent.click(screen.getByText('Sell'));
  expect(mockSetOpenNewListing).toBeCalled();
});

test('Search with Enter', async () => {
  render(<SearchFilter setSearch={mockSetSearch}/>);
  const input = screen.getByLabelText('search');
  fireEvent.change(input, {target: {value: 'hello'}});
  fireEvent.keyDown(input, {key: 'Enter'});
  expect(mockSetSearch).toBeCalled();
});

test('Search with random key', async () => {
  render(<SearchFilter setSearch={mockSetSearch}/>);
  const input = screen.getByLabelText('search');
  fireEvent.change(input, {target: {value: 'hello'}});
  fireEvent.keyDown(input, {key: 'G'});
  expect(mockSetSearch).not.toBeCalled();
});
