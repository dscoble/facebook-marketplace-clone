import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen, waitFor} from '@testing-library/react';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import NewListing from '../NewListing';

const URL = 'http://localhost:3010/v0/listings';
const mockSetOpenNewListing = jest.fn();
const mockCat = [
  {
    'id': 'c5f2f682-79c1-4c12-98e4-7db79f0df9c7',
    'category': {
      'name': 'Vehicles',
      'filters': [
        {
          'condition': ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'],
        },
      ],
    },
  },
];
const user = {
  'firstname': 'test',
  'lastname': 'test',
  'email': 'testtest@test.com',
  'accessToken': 'dummy',
};

localStorage.setItem('user', JSON.stringify(user));

const server = setupServer(
  rest.post(URL, (req, res, ctx) => {
    return res(ctx.json({response: 'a response'}));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('New Listing Renders', async () => {
  render(
    <NewListing
    />);
});

test('Enter input', async () => {
  render(<NewListing loggedIn={true}/>);
  const input = screen.getByLabelText('title');
  fireEvent.change(input, {target: {value: 'title'}});
});

test('Trigger sub menu input', async () => {
  render(
    <NewListing
      categories={mockCat}
      loggedIn={true}
    />);
  const input = screen.getByLabelText('category');
  fireEvent.click(input);
  const cat = screen.getByText(mockCat[0].category.name);
  expect(cat).not.toBeNull();
});

test('close new listing', async () => {
  render(
    <NewListing
      openNewListing={true}
      setOpenNewListing={mockSetOpenNewListing}
      categories={mockCat}
      loggedIn={true}
    />);
  const close = screen.getByLabelText('close-new-listing');
  fireEvent.click(close);
});

test('post new listing', async () => {
  render(
    <NewListing
      openNewListing={true}
      setOpenNewListing={mockSetOpenNewListing}
      categories={mockCat}
      loggedIn={true}
    />);
  fireEvent.change(screen.getByLabelText('title'), {target: {value: 'title'}});
  fireEvent.change(
    screen.getByLabelText('description'), {target: {value: 'description'}});
  fireEvent.change(
    screen.getByLabelText('images'), {target: {value: 'images'}});
  fireEvent.change(
    screen.getByLabelText('location'), {target: {value: 'location'}});
  fireEvent.change(
    screen.getByLabelText('condition'), {target: {value: 'condition'}});
  fireEvent.change(
    screen.getByLabelText('category'), {target: {value: 'category'}});
  fireEvent.change(
    screen.getByLabelText('price'), {target: {value: 'price'}});
  const submit = screen.getByLabelText('submit');
  fireEvent.click(submit);
  await waitFor(() => expect(mockSetOpenNewListing).toHaveBeenCalledTimes(1));
});

test('Click server error', async () => {
  server.use(
    rest.post(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(
    <NewListing
      openNewListing={true}
      setOpenNewListing={mockSetOpenNewListing}
      categories={mockCat}
      loggedIn={true}
    />);
  fireEvent.change(screen.getByLabelText('title'), {target: {value: 'title'}});
  fireEvent.change(
    screen.getByLabelText('description'), {target: {value: 'description'}});
  fireEvent.change(
    screen.getByLabelText('images'), {target: {value: 'images'}});
  fireEvent.change(
    screen.getByLabelText('location'), {target: {value: 'location'}});
  fireEvent.change(
    screen.getByLabelText('condition'), {target: {value: 'condition'}});
  fireEvent.change(
    screen.getByLabelText('category'), {target: {value: 'category'}});
  const submit = screen.getByLabelText('submit');
  fireEvent.click(submit);
  await waitFor(() => screen.getByText('Error submitting'));
});
