import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import TopBar from '../TopBar';

const mockSetOpenLogin = jest.fn();

/**
 * Credits: Asgn5 common.js
 * @param {*} width
 */
function setWidth(width) {
  global.innerWidth = width;
  act(() => {
    global.dispatchEvent(new Event('resize'));
  });
}

test('Top Bar Renders', async () => {
  render(<TopBar/>);
});

test('Display Mobile LogIn, Check Drawer Open', async () => {
  setWidth(600);
  render(<TopBar setOpenLogin={mockSetOpenLogin}/>);
  const login = screen.getByLabelText('log in');
  fireEvent.click(login);
  expect(mockSetOpenLogin).toBeCalled();
});


test('Display Desktop LogIn', async () => {
  setWidth(700);
  render(<TopBar setOpenLogin={mockSetOpenLogin}/>);
  const login = screen.getByLabelText('log in');
  fireEvent.click(login);
  expect(mockSetOpenLogin).not.toBeCalled();
});
