import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import {screen, waitFor} from '@testing-library/react';
import CategoryHeader from '../CategoryHeader';

const URL = 'http://localhost:3010/v0/categories';
const mockSelectedCat = {
  'id': 'd5f2f682-79c1-4c12-98e4-7db79f0df9c7',
  'category': {
    'name': 'Vehicles',
    'filters': [
      {
        'condition': ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'],
      },
    ],
  },
};
const mockSubCategories = [
  {
    'id': 'c5f2f682-79c1-4c12-98e4-7db79f0df9c7',
    'category': {
      'name': 'Cars',
      'filters': [
        {
          'condition': ['New', 'Used - Like New', 'Used - Good', 'Used - Fair'],
        },
      ],
    },
  },
];
const mockSetSelectedSubCat = jest.fn();
const mockSetSubcateogires = jest.fn();

const server = setupServer(
  rest.get(URL, (req, res, ctx) => {
    return res(ctx.json(mockSubCategories));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('Category Header Renders', async () => {
  render(
    <CategoryHeader
      subcategories={[]}
      selectedCat = {mockSelectedCat}
      setSubcategories = {mockSetSubcateogires}
      inCat = {true}
    />);
  await waitFor(() =>
    expect(mockSetSubcateogires).toBeCalled());
});

test('Server Error', async () => {
  server.use(
    rest.get(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );
  render(
    <CategoryHeader
      subcategories={[]}
      selectedCat = {mockSelectedCat}
      setSubcategories = {mockSetSubcateogires}
      inCat = {true}
    />);
  await waitFor(() =>
    expect(mockSetSubcateogires).toBeCalled());
});


test('Category Header Subcategory click success', async () => {
  render(
    <CategoryHeader
      subcategories={mockSubCategories}
      selectedCat = {mockSelectedCat}
      setSubcategories = {mockSetSubcateogires}
      setSelectedSubCat = {mockSetSelectedSubCat}
      inCat = {true}
    />);
  const button = screen.getByText(mockSubCategories[0].category.name);
  fireEvent.click(button);
  expect(mockSetSelectedSubCat).toBeCalled();
});

test('Category Header Subcategory not in cat', async () => {
  render(
    <CategoryHeader
      subcategories={mockSubCategories}
      selectedCat = {mockSelectedCat}
      setSubcategories = {mockSetSubcateogires}
      setSelectedSubCat = {mockSetSelectedSubCat}
      inCat = {false}
    />);
});
