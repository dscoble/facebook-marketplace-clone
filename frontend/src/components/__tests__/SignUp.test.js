import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';
import {screen, waitFor} from '@testing-library/react';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import SignUp from '../SignUp';

const URL = 'http://localhost:3010/v0/signup';
const mockUser = {
  'id': 'id',
  'accessToken': 'dummy',
  'firstname': 'firstname',
  'lastname': 'lastname',
};

// const mockSetSignedIn = jest.fn();
const mockSetOpenSignup = jest.fn();

const server = setupServer(
  rest.post(URL, (req, res, ctx) => {
    return res(ctx.json(mockUser));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('SignUp Renders', async () => {
  render(
    <SignUp
      openSignup={true}
    />);
});

test('SignUp Form & Submit', async () => {
  // render signup component
  render(
    <SignUp
      openSignup={true}
      setOpenSignup={mockSetOpenSignup}
    />);

  // Enter values into input fields
  const email = screen.getByLabelText('email');
  fireEvent.change(email, {target: {value: 'email'}});
  const firstname = screen.getByLabelText('firstname');
  fireEvent.change(firstname, {target: {value: 'firstname'}});
  const lastname = screen.getByLabelText('lastname');
  fireEvent.change(lastname, {target: {value: 'lastname'}});
  const pass = screen.getByLabelText('password');
  fireEvent.change(pass, {target: {value: 'pass'}});

  // Submit the form
  const submit = screen.getByLabelText('submit');
  fireEvent.click(submit);

  // Wait for mocked function to be called
  await waitFor(() => expect(mockSetOpenSignup).toHaveBeenCalledTimes(1));
});

test('SignUP Server error', async () => {
  // mock server error
  server.use(
    rest.post(URL, (req, res, ctx) => {
      return res(ctx.status(500));
    }),
  );

  // render the login component
  render(
    <SignUp
      openSignup={true}
      setOpenSignup={mockSetOpenSignup}
    />);

  // Enter values into input fields
  const email = screen.getByLabelText('email');
  fireEvent.change(email, {target: {value: 'email'}});
  const firstname = screen.getByLabelText('firstname');
  fireEvent.change(firstname, {target: {value: 'firstname'}});
  const lastname = screen.getByLabelText('lastname');
  fireEvent.change(lastname, {target: {value: 'lastname'}});
  const pass = screen.getByLabelText('password');
  fireEvent.change(pass, {target: {value: 'pass'}});

  // submit the form
  const submit = screen.getByLabelText('submit');
  fireEvent.click(submit);

  // wait for the error message to appear
  await waitFor(() => screen.getByText('Error signing up, please try again'));
});
