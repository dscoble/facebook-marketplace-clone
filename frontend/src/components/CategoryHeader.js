import React, {useEffect} from 'react';
import {Grid, Button, Box} from '@material-ui/core';

/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function CategoryHeader(props) {
  const subcat = props.selectedCat;
  const setsubcat = props.setSubcategories;
  useEffect(() => {
    if (subcat.id !== undefined) {
      fetch(`http://localhost:3010/v0/categories?subcategories=${props.selectedCat.id}`, {
        method: 'GET',
      })
        .then((res) => {
          if (!res.ok) throw res;
          return res.json();
        })
        .then((json) => {
          setsubcat(json);
        })
        .catch((err) => {
          console.log(err);
          setsubcat([{category: {name: 'Error fetching'}}]);
        });
    }
  }, [props.selectedCat, subcat, setsubcat]);
  return (
    <div>
      {props.inCat &&
        <div>
          <p style={{color: 'grey'}}>
            Marketplace - {props.selectedCat.category.name}
          </p>
          <h2>
            {props.selectedCat.category.name}
          </h2>
          <Box style={{overflow: 'auto'}}>
            <Grid container spacing={2} wrap='nowrap' style={{width: 500}}>
              {props.subcategories.map((subcat) => (
                <Button style = {{backgroundColor: '#ddd', borderRadius: 30,
                  fontWeight: 'bold', margin: 10, fontSize: 10}}
                onClick = {() => props.setSelectedSubCat(subcat)}
                key={subcat.id}>
                  {subcat.category.name}
                </Button>
              ))}
            </Grid>
          </Box>
        </div>
      }
    </div>
  );
}

export default CategoryHeader;
