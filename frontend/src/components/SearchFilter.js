import React from 'react';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import {Button} from '@material-ui/core';


/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function SearchFilter(props) {
  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      props.setSearch(e.target.value);
    }
  };
  return (
    <div>
      <div style={{marginTop: 10}}>
        <Button
          aria-label="user"
          style={{backgroundColor: '#dadada',
            borderRadius: 40, overflow: 'hidden'}}
          onClick = {() => props.setUserDrawer(true)}>
          <PersonOutlineIcon />
        </Button>
        <Button style={{backgroundColor: '#dadada',
          borderRadius: 40, overflow: 'hidden', height: 36,
          marginLeft: 10}}
        onClick={() => props.setOpenNewListing(true)}
        >
          <p style={{fontWeight: 'bold', margin: 0}}>
            Sell
          </p>
        </Button>
        <Button style={{backgroundColor: '#dadada',
          borderRadius: 40, overflow: 'hidden', height: 36,
          marginLeft: 10}}
        onClick={() => props.toggleDrawer(true)}>
          <p style={{fontWeight: 'bold', margin: 0}}>
            All Categories
          </p>
        </Button>
      </div>
      <div style={{marginTop: 10}}>
        <input aria-label='search'
          defaultValue='Search marketplace'
          type='text' onKeyDown={(e) => handleKeyPress(e)}
          style={{
            backgroundColor: '#dadada', borderRadius: 10,
            overflow: 'hidden', height: 25, paddingLeft: 5, border: 'none',
            width: '100%',
          }}/>
      </div>
      <div style={{marginTop: 10}}>
        <Button style={{backgroundColor: '#99ddff',
          color: 'blue', fontSize: 10, marginLeft: 50}}>
          Santa Cruz - 40mi
        </Button>
        <Button style={{backgroundColor: '#99ddff',
          color: 'blue', fontSize: 10, marginLeft: 80}}
        onClick={() => props.setFilterDrawer(true)}>
          Filters
        </Button>
      </div>
    </div>
  );
}

export default SearchFilter;
