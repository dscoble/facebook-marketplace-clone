/**
 * Credits:
 * Authenticated Books example
 * https://mui.com/components/drawers/
 * https://mswjs.io/docs/getting-started/mocks
 * https://testing-library.com/docs/react-testing-library/example-intro/
 */
import React, {useState} from 'react';
import TopBar from './TopBar';
import WelcomeMessage from './WelcomeMessage';
import SearchFilter from './SearchFilter';
import CategoryChooser from './CategoryChooser';
import CategoryHeader from './CategoryHeader';
import Listings from './Listings';
import ListingsPopUp from './ListingPopUp';
import Filters from './Filters';
import NewListing from './NewListing';
import Login from './Login';
import SignUp from './SignUp';
import UserListings from './UserListings';

/**
 * Simple component with no state.
 *
 * @return {object} JSX
 */
function App() {
  const defaultCategory = {category: {name: 'items'}};
  const [drawer, setDrawer] = useState(false);
  const [categories, setCategories] = useState([]);
  const [selectedCat, setSelectedCat] = useState(defaultCategory);
  const [selectedSubCat, setSelectedSubCat] = useState(null);
  const [inCat, setInCat] = useState(false);
  const [openListing, setOpenListing] = useState(false);
  const [activeListing, setActiveListing] = useState(null);
  const [subcategories, setSubcategories] = useState([]);
  const [filterDrawer, setFilterDrawer] = useState(false);
  const [search, setSearch] = useState(null);
  const [openNewListing, setOpenNewListing] = useState(false);
  const [openLogin, setOpenLogin] = useState(false);
  const [openSignup, setOpenSignup] = useState(false);
  const [userDrawer, setUserDrawer] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);

  return (
    <div style={{fontFamily: 'Helvetica'}}>
      <TopBar
        setLoggedIn = {setLoggedIn}
        setOpenLogin={setOpenLogin}/>
      <WelcomeMessage
        selectedCat = {selectedCat}
        setOpenLogin={setOpenLogin}
      />
      <CategoryHeader
        selectedCat = {selectedCat}
        setSubcategories = {setSubcategories}
        setSelectedSubCat = {setSelectedSubCat}
        subcategories = {subcategories}
        setSelectedCat = {setSelectedCat}
        inCat = {inCat}
      />
      <SearchFilter drawer = {drawer} toggleDrawer = {setDrawer}
        filterDrawer = {filterDrawer} setFilterDrawer = {setFilterDrawer}
        setSearch = {setSearch} openNewListing = {openNewListing}
        setOpenNewListing = {setOpenNewListing}
        setUserDrawer = {setUserDrawer}/>
      <UserListings userDrawer={userDrawer} setUserDrawer={setUserDrawer}
        handleActiveListing = {setActiveListing}
        handleOpenListing = {setOpenListing}
        openListing = {openListing}
        loggedIn = {loggedIn}
      />
      <CategoryChooser drawer = {drawer} toggleDrawer = {setDrawer}
        categories = {categories} setCategories = {setCategories}
        selectedCat = {selectedCat} setSelectedCat = {setSelectedCat}
        setInCat = {setInCat}/>
      <Listings
        activeListing = {activeListing}
        handleActiveListing = {setActiveListing}
        handleOpenListing = {setOpenListing}
        openListing = {openListing}
        selectedCat = {selectedCat}
        selectedSubCat = {selectedSubCat}
        search = {search}
      />
      <ListingsPopUp
        openListing = {openListing}
        handleOpenListing = {setOpenListing}
        activeListing = {activeListing}
        loggedIn = {loggedIn}
      />
      <Filters filterDrawer = {filterDrawer}
        setFilterDrawer={setFilterDrawer} selectedCat={selectedCat}/>
      <NewListing
        openNewListing = {openNewListing}
        setOpenNewListing = {setOpenNewListing}
        categories = {categories}
        loggedIn = {loggedIn}
      />
      <Login
        openLogin={openLogin}
        setOpenLogin={setOpenLogin}
        setOpenSignup={setOpenSignup}
        setLoggedIn = {setLoggedIn}
      />
      <SignUp
        openSignup={openSignup}
        setOpenSignup={setOpenSignup}
        setOpenLogin={setOpenLogin}
      />
    </div>
  );
}

export default App;
