import React from 'react';
import {Drawer, Toolbar, Button, Grid} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function Filters(props) {
  return (
    <Drawer open={props.filterDrawer} anchor={'bottom'}
      variant='persistent' style={{backgroundColor: 'white'}}
      BackdropProps={{invisble: true}}>
      <div style={{height: '95%'}}>
        <Toolbar>
          <Grid justifyContent='space-between'
            container
            spacing={0}>
            <p key={'0'}style={{color: 'black',
              fontWeight: 'bold'}}> Filters </p>
            <Button key = {'1'}style={{marginTop: 8, backgroundColor: '#dadada',
              borderRadius: 50, overflow: 'hidden',
              height: 35, width: 35}} role='closefilter'
            onClick = {() => props.setFilterDrawer(false)}>
              <CloseIcon />
            </Button>
          </Grid>
        </Toolbar>
      </div>
    </Drawer>
  );
}

export default Filters;
