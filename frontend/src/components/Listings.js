import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Grid,
  Card,
  CardContent,
  Typography,
  CardMedia,
  CardActionArea,
} from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  media: {
    height: 250,
  },
}));

/**
 * @param {*} props
 * @return {Object} JSX
 *
 * Credits:
 * https://codesandbox.io/s/8rbh7?file=/demo.js:521-555
 * https://stackoverflow.com/questions/51595156/three-cards-in-a-row-instead-of-all-cards-in-one-column
 */
function Listings(props) {
  const classes = useStyles();
  const [listings, setListings] = useState([]);

  const showListing = (listing) => {
    props.handleActiveListing(listing);
    props.handleOpenListing(!props.openListing);
  };

  useEffect(() => {
    const getListings = () => {
      fetch('http://localhost:3010/v0/listings', {
        method: 'GET',
      })
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setListings(json);
        })
        .catch((error) => {
          setListings('Failed to fetch');
        });
    };
    getListings();
  }, []);

  useEffect(() => {
    if (props.selectedCat != null) {
      fetch(`http://localhost:3010/v0/categories/${props.selectedCat.id}`, {
        method: 'GET',
      })
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setListings(json);
        })
        .catch((error) => {
          setListings('Failed to fetch');
        });
    }
  }, [props.selectedCat]);

  useEffect(() => {
    if (props.selectedSubCat != null) {
      fetch(`http://localhost:3010/v0/listings?subcategory=${props.selectedSubCat.id}`, {
        method: 'GET',
      })
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setListings(json);
        })
        .catch((error) => {
          setListings('Failed to fetch');
        });
    }
  }, [props.selectedSubCat]);

  useEffect(() => {
    if (props.search != null) {
      fetch(`http://localhost:3010/v0/search?substring=${props.search}`, {
        method: 'GET',
      })
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setListings(json);
        })
        .catch((error) => {
          setListings('Failed to fetch');
        });
    }
  }, [props.search]);

  const renderListings = () => {
    if (listings === 'Failed to fetch') {
      return (
        <div>no listings</div>
      );
    }
    return (
      <Grid
        container
        spacing={2}
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start"
      >
        {listings.map((listing) => (
          <Grid item xs={6} sm={6} md={3} key={listing.id}>
            <Card>
              <CardActionArea
                onClick={() => showListing(listing)}
              >
                <CardMedia
                  className={classes.media}
                  image={listing.listing.images[0]}
                />
                <CardContent
                  aria-label={`listing-${listing['id']}`}
                  id={`i${listing['id']}`}
                >
                  <Typography variant="h5" gutterBottom>
                    {listing.listing.price}
                  </Typography>
                  <Typography variant="h6" gutterBottom>
                    {listing.listing.title}
                  </Typography>
                  <Typography variant="subtitle1" gutterBottom>
                    {listing.listing.location}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
    );
  };

  return (
    <div className={classes.root}>
      {renderListings()}
    </div>
  );
};

export default Listings;
