import React, {useState} from 'react';
import {Toolbar,
  Grid, Button, Drawer, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

// import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    width: '100%',
    height: '100%',
  },
}));
/**
 * Simple component with no state.
 * @param {object} props
 * @return {object} JSX
 */
function Login(props) {
  const classes = useStyles();
  const [user, setUser] = useState({email: '', password: ''});
  const [error, setError] = useState('');

  const handleInputChange = (event) => {
    const {value, name} = event.target;
    const u = user;
    u[name] = value;
    setUser(u);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    fetch('http://localhost:3010/v0/authenticate', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        if (!res.ok) {
          throw res;
        }
        return res.json();
      })
      .then((json) => {
        localStorage.setItem('user', JSON.stringify(json));
        props.setLoggedIn(true);
        props.setOpenLogin(false);
        setError('');
      })
      .catch((err) => {
        console.log(err);
        setError('Error logging in, please try again');
      });
  };
  // console.log(user);
  return (
    <Drawer open={props.openLogin} anchor={'bottom'}
      variant='persistent' style={{backgroundColor: 'white'}}
      BackdropProps={{invisble: true}}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Toolbar variant="dense">
        <Grid
          justifyContent="center"
          container
          spacing={10}
        >
          <Grid item>
            <Typography style={{
              color: 'blue', fontWeight: 'bold',
              marginTop: 3,
            }} variant="h6"
            color="inherit" component="div">
                            facebook
            </Typography>
          </Grid>
        </Grid>
      </Toolbar>
      <div style={{textAlign: 'center'}}>
        <form onSubmit={onSubmit} style={{
          marginTop: 60,
          display: 'inline-block',
        }}>
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            margin: 10, padding: 2,
          }}
          aria-label="email"
          type="email"
          name="email"
          placeholder="Email"
          onChange={handleInputChange}
          required />
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            margin: 10, padding: 2,
          }}
          aria-label="password"
          type="password"
          name="password"
          placeholder="Password"
          onChange={handleInputChange}
          required />
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            backgroundColor: 'blue', color: 'white',
            padding: 2,
          }} aria-label="submit" type="submit" value="Log In"/>
          {error &&
            <div style={{color: 'red'}}>{error}</div>
          }
        </form>
        <div>
          <hr style={{width: '180px'}}/>
          <Button onClick={()=> props.setOpenSignup(true)} style={{
            width: '400px', height: '30px', borderRadius: '6px',
            backgroundColor: 'green', color: 'white',
            padding: 2,
          }}>Create An Account
          </Button>
        </div>
      </div>
    </Drawer>
  );
}

export default Login;
