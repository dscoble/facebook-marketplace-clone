import React, {useEffect, useState} from 'react';
import {List, ListItem, Button,
  Drawer, AppBar, Toolbar, Grid} from '@material-ui/core';
import ListItemButton from '@mui/material/ListItemButton';
import CloseIcon from '@material-ui/icons/Close';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    width: '100%',
  },
}));

/**
 * Simple component with no state
 * @param {object} props props
 * @return {object} JSX
 */
function CategoryChooser(props) {
  const c = useStyles();
  const [error, setError] = useState('');
  useEffect(() => {
    const setcat = props.setCategories;
    fetch('http://localhost:3010/v0/categories', {
      method: 'GET',
    })
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((json) => {
        setcat(json);
      })
      .catch((err) => {
        setError('Error fetching categories');
      });
  }, [props.setCategories]);

  /**
   * Selects category and closes sidebar
   * @param {object} props exists
   * @param {string} cat exists
   */
  function selectAndClose(props, cat) {
    props.setSelectedCat(cat);
    props.toggleDrawer(false);
    props.setInCat(true);
  }
  console.log(props.categories);
  return (
    <Drawer anchor = {'left'} open = {props.drawer}
      variant='persistent'
      BackdropProps={{invisble: true}}
      classes={{
        paper: c.drawerPaper,
      }}
    >
      <AppBar style={{backgroundColor: 'white', zindex: 10000}}>
        <Toolbar>
          <Grid justifyContent='space-between'
            container
            spacing={0}>
            <p key={'0'}style={{color: 'black',
              fontWeight: 'bold'}}> Select Category </p>
            <Button aria-label='close-cat-drawer'
              key = {'1'}style={{marginTop: 8, backgroundColor: '#dadada',
                borderRadius: 50, overflow: 'hidden',
                height: 35, width: 35}} role='close'
              onClick = {() => props.toggleDrawer(false)}>
              <CloseIcon />
            </Button>
          </Grid>
        </Toolbar>
      </AppBar>
      <List style={{marginTop: 50}}>
        {props.categories.map((cat) => (
          <ListItem key={cat.id}>
            <ListItemButton key={cat.category.name}
              onClick={() => selectAndClose(props, cat)}
              style = {{padding: 10, textAlign: 'left',
                paddingRight: 240}}>
              {cat.category.name}
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      {error &&
        <div style={{color: 'red'}}>{error}</div>
      }
    </Drawer>
  );
}

export default CategoryChooser;
