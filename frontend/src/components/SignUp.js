import React, {useState} from 'react';
import {AppBar, Toolbar, Typography, Grid, Drawer} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

// import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    width: '100%',
    height: '100%',
  },
}));

/**
 * Simple component with no state.
 * @param {object} props
 * @return {object} JSX
 */
function SignUp(props) {
  const classes = useStyles();
  const [user, setUser] = React.useState({
    firstname: '', lastname: '', email: '', password: '',
  });
  const [error, setError] = useState('');
  // const history = useHistory();

  const handleInputChange = (event) => {
    const {value, name} = event.target;
    const u = user;
    u[name] = value;
    setUser(u);
    // console.log(value);
    // console.log(name);
  };

  // console.log(user);
  const onSubmit = (event) => {
    // console.log(user);
    event.preventDefault();
    fetch('http://localhost:3010/v0/signup', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        // console.log(res);
        if (!res.ok) {
          throw res;
        }
        return res.json();
        // console.log(res.json);
      })
      .then((json) => {
        // setUser(json.user);
        localStorage.setItem('user', JSON.stringify(json));
        props.setOpenSignup(false);
      })
      .catch((err) => {
        console.log(err);
        setError('Error signing up, please try again');
      });
  };
  return (
    <Drawer open={props.openSignup} anchor={'bottom'}
      variant='persistent' style={{backgroundColor: 'white'}}
      BackdropProps={{invisble: true}}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <><AppBar style={{backgroundColor: 'white', justify: 'center'}}>
        <Toolbar variant="dense">
          <Grid
            justifyContent="center"
            container
            spacing={10}
          >
            <Grid item>
              <Typography style={{
                color: 'blue', fontWeight: 'bold',
                marginTop: 3,
              }} variant="h6"
              color="inherit" component="div">
                              facebook
              </Typography>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <div style={{textAlign: 'center'}}>
        <form onSubmit={onSubmit} style={{
          marginTop: 60,
          display: 'inline-block',
        }}>
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            margin: 10, padding: 2,
          }}
          aria-label="firstname"
          type="text"
          name="firstname"
          placeholder="First Name"
          onChange={handleInputChange}
          required />
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            margin: 10, padding: 2,
          }}
          aria-label="lastname"
          type="text"
          name="lastname"
          placeholder="Last Name"
          onChange={handleInputChange}
          required />
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            margin: 10, padding: 2,
          }}
          aria-label="email"
          type="email"
          name="email"
          placeholder="Email"
          onChange={handleInputChange}
          required />
          {/* <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            margin: 10, padding: 2,
          }}
          type="tel"
          name="phonenumber"
          placeholder="Phone Number"
          onChange={handleInputChange}
          required /> */}
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            margin: 10, padding: 2,
          }}
          aria-label="password"
          type="password"
          name="password"
          placeholder="Password"
          onChange={handleInputChange}
          required />
          <input style={{
            width: '400px', height: '30px', borderRadius: '6px',
            backgroundColor: 'green', color: 'white',
            padding: 2,
          }} aria-label="submit" type="submit" value="Join"/>
          {error &&
            <div style={{color: 'red'}}>{error}</div>
          }
        </form>
      </div></>
    </Drawer>
  );
}

export default SignUp;
