import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {AppBar, Toolbar, Grid, Button, Drawer, List,
  ListItem, Card, CardActionArea,
  CardContent, Typography, CardMedia} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  media: {
    height: 250,
  },
  card: {
    width: '100%',
  },
}));

/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function UserListings(props) {
  const classes = useStyles();
  const [error, setError] = useState('');
  const [usersListings, setUsersListings] = useState([]);

  useEffect(() => {
    const useritem = localStorage.getItem('user');
    const user = JSON.parse(useritem);
    const getListings = () => {
      fetch('http://localhost:3010/v0/listings/user/' + user.id, {
        method: 'GET',
        headers: new Headers({
          'Authorization': `Bearer ${user.accessToken}`,
          'Content-Type': 'application/json',
        }),
      })
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setUsersListings(json);
        })
        .catch((error) => {
          setError('Failed to fetch');
        });
    };
    if (props.loggedIn) {
      getListings();
    }
    return () => {
      setUsersListings([]);
    };
  }, [props.userDrawer, props.loggedIn]);

  return (
    <Drawer anchor = {'bottom'} open = {props.userDrawer}>
      <AppBar style={{backgroundColor: 'white', zindex: 10000}}>
        <Toolbar>
          <Grid justifyContent='space-between'
            container
            spacing={0}>
            <p key={'0'}style={{color: 'black',
              fontWeight: 'bold'}}> Your Listings </p>
            <Button aria-label='close-user-drawer'
              key = {'1'}style={{marginTop: 8, backgroundColor: '#dadada',
                borderRadius: 50, overflow: 'hidden',
                height: 35, width: 35}} role='close'
              onClick = {() => props.setUserDrawer(false)}>
              <CloseIcon />
            </Button>
          </Grid>
        </Toolbar>
      </AppBar>
      {props.loggedIn ?
        <List style={{}}>
          {usersListings.map((listing) => (
            <ListItem key={listing}>
              <Card className={classes.card}>
                <CardActionArea>
                  <CardContent
                    aria-label={`listing-${listing['id']}`}
                    id={`i${listing['id']}`}
                  >
                    <CardMedia
                      className={classes.media}
                      image={listing.listing.images[0]}
                    />
                    <Typography variant="h5" gutterBottom>
                      {listing.listing.price}
                    </Typography>
                    <Typography variant="h6" gutterBottom>
                      {listing.listing.title}
                    </Typography>
                    <Typography variant="subtitle1" gutterBottom>
                      {listing.listing.location}
                    </Typography>
                    <Typography variant="subtitle1" gutterBottom>
                      {listing.listing.description}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </ListItem>
          ))}
        </List> :
        <div>Please Log In</div>
      }
      {error &&
        <div style={{color: 'red'}}>{error}</div>
      }
    </Drawer>
  );
}

export default UserListings;
