import React, {useState} from 'react';
import {Drawer, Toolbar, Button, Grid} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import {makeStyles} from '@material-ui/core/styles';

import NewListingSubMenu from './NewListingSubMenu';

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    width: '100%',
    height: '100%',
  },
  form: {
    textAlign: 'center',
  },
  input: {
    display: 'block',
    marginTop: '20px',
    width: '90%',
    borderRadius: '5px',
    height: '40px',
    margin: 'auto',
    borderStyle: 'solid',
    paddingLeft: '10px',
    borderColor: '#dadada',
  },
  error: {
    marginTop: '20px',
    textAlign: 'center',
  },
  submit: {
    marginTop: '20px',
    width: '400px',
    height: '30px',
    borderRadius: '6px',
    backgroundColor: 'blue',
    color: 'white',
    padding: 2,
  },
  label: {
    display: 'inline-block',
    paddingTop: '20px',
  },
}));

/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function NewListing(props) {
  const classes = useStyles();
  const [openNewListingSubMenu, setOpenNewListingSubMenu] = useState(false);
  const [selectedCat, setSelectedCat] = useState('');
  const [newListingSubMenuType, setnewListingSubMenuType] = useState('');
  const [error, setError] = useState('');
  const [listing, setListing] = useState({
    description: '',
    price: '',
    title: '',
    location: '',
    images: [],
    condition: '',
    subcategory: '',
  });

  const handleInputChange = (event) => {
    const {value, name} = event.target;
    const u = listing;
    u[name] = value;
    setListing(u);
  };

  const handleSubMenu = (e) => {
    setOpenNewListingSubMenu(true);
    setnewListingSubMenuType(e.target.name);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    listing['images'] = [listing['images']];
    const item = localStorage.getItem('user');
    const user = JSON.parse(item);
    fetch('http://localhost:3010/v0/listings', {
      method: 'POST',
      headers: new Headers({
        'Authorization': `Bearer ${user.accessToken}`,
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify({
        'member': user.id,
        'category': selectedCat,
        'listing': listing,
      }),
    })
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((json) => {
        const list = {
          description: '',
          price: '',
          title: '',
          location: '',
          images: [],
          condition: '',
          subcategory: '',
        };
        setListing(list);
        props.setOpenNewListing(false);
      })
      .catch((err) => {
        setError('Error submitting');
      });
  };

  return (
    <div>
      <Drawer open={props.openNewListing} anchor={'bottom'}
        variant='persistent' style={{backgroundColor: 'white'}}
        BackdropProps={{invisble: true}}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar>
          <Grid justifyContent='space-between'
            container
            spacing={0}>
            <Button key = {'3'} aria-label='close-new-listing'
              style={{marginTop: 8, backgroundColor: '#dadada',
                borderRadius: 50, overflow: 'hidden',
                height: 35, width: 35}}
              onClick = {() => props.setOpenNewListing(false)}>
              <CloseIcon />
            </Button>
          </Grid>
        </Toolbar>
        {props.loggedIn ?
          <form onSubmit={onSubmit} className={classes.form}>
            <input
              type="text"
              name="images"
              aria-label="images"
              placeholder="images"
              onChange={handleInputChange}
              className={classes.input}
              required
            />
            <input
              type="text"
              aria-label="title"
              name="title"
              placeholder="title"
              onChange={handleInputChange}
              className={classes.input}
              required
            />
            <input
              type="text"
              aria-label="price"
              name="price"
              placeholder="price"
              onChange={handleInputChange}
              className={classes.input}
              required
            />
            <input
              type="text"
              aria-label="location"
              name="location"
              placeholder="location"
              onChange={handleInputChange}
              className={classes.input}
              required
            />
            <label className={classes.label} htmlFor="category">Category</label>
            <input
              type="text"
              aria-label="category"
              name="category"
              placeholder={listing['subcategory']}
              onClick={handleSubMenu}
              className={classes.input}
            />
            <label
              className={classes.label}
              htmlFor="condition"
            >
              Condition
            </label>
            <input
              type="text"
              aria-label="condition"
              name="condition"
              placeholder={listing['condition']}
              onClick={handleSubMenu}
              className={classes.input}
            />
            <NewListingSubMenu
              openNewListingSubMenu = {openNewListingSubMenu}
              setOpenNewListingSubMenu = {setOpenNewListingSubMenu}
              newListingSubMenuType = {newListingSubMenuType}
              setnewListingSubMenuType = {setnewListingSubMenuType}
              categories = {props.categories}
              listing = {listing}
              setListing = {setListing}
              setSelectedCat = {setSelectedCat}
            />
            <input
              type="text"
              aria-label="description"
              name="description"
              placeholder="description"
              onChange={handleInputChange}
              className={classes.input}
              required
            />
            <input
              className={classes.submit}
              aria-label="submit"
              type="submit"
              value="Submit"
            />
            {error &&
              <div>Error submitting</div>
            }
          </form> :
          <div className={classes.error}>Please Log in</div>
        }
      </Drawer>
    </div>
  );
}

export default NewListing;
