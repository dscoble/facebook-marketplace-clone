import React, {useEffect, useState} from 'react';
import {
  Drawer,
  Toolbar,
  Grid,
  Button,
  Card,
  CardMedia,
  CardContent,
  Typography,
} from '@material-ui/core/';
import {makeStyles} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  media: {
    height: '200px',
  },
  drawerPaper: {
    width: '100%',
    height: '100%',
  },
  input: {
    width: '100%',
    borderRadius: '10px',
    border: 'none',
    backgroundColor: '#dadada',
    height: '25px',
  },
}));

/**
 * @param {*} props
 * @return {Object} JSX
 *
 * https://www.positronx.io/create-react-modal-popup-with-material-ui/
*/
function ListingsPopUp(props) {
  const classes = useStyles();
  const [response, setResponse] = useState(
    {firstname: 'test', lastname: 'test', text: ''},
  );
  const [listing, setListing] = useState({});
  const handleClose = () => {
    props.handleOpenListing(!props.openListing);
  };
  const handleInputChange = (e) => {
    setResponse['text'] = e.target.value;
  };
  const handleKeyPress = (e) => {
    const item = localStorage.getItem('user');
    const user = JSON.parse(item);
    setResponse['firstname'] = user.firstname;
    setResponse['lastname'] = user.lastname;
    if (e.key === 'Enter') {
      fetch('http://localhost:3010/v0/responses', {
        method: 'POST',
        headers: new Headers({
          'Authorization': `Bearer ${user.accessToken}`,
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
          'listing': props.activeListing.id,
          'response': response,
        }),
      })
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setResponse({firstname: 'test', lastname: 'test', text: ''});
          props.handleOpenListing(false);
        })
        .catch((error) => {
          setResponse('Failed to send');
        });
    }
  };
  const getResponse = () => {
    if (response === 'Failed to send') {
      return (<div>Failed to Send</div>);
    }
  };
  useEffect(() => {
    if (props.activeListing != null) {
      fetch(`http://localhost:3010/v0/listings/${props.activeListing.id}`, {
        method: 'GET',
      })
        .then((response) => {
          if (!response.ok) {
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          setListing(json);
        })
        .catch((error) => {
          setListing('Failed to get listing');
        });
    }
    return () => {
      setListing({});
    };
  }, [props.activeListing]);
  return (
    <div>
      {props.openListing &&
        <Drawer open={props.openListing} anchor={'bottom'}
          variant='persistent' style={{backgroundColor: 'white'}}
          BackdropProps={{invisble: true}}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <Toolbar>
            <Grid justifyContent='space-between'
              container
              spacing={0}>
              <Button
                key = {'1'}
                aria-label='close-listing'
                style={{marginTop: 8, backgroundColor: '#dadada',
                  borderRadius: 50, overflow: 'hidden',
                  height: 35, width: 35}}
                onClick = {() => handleClose()}>
                <CloseIcon />
              </Button>
            </Grid>
          </Toolbar>
          {listing !== 'Failed to get listing' && listing.listing &&
          <Card>
            <CardMedia
              className={classes.media}
              image={listing.listing.images[0]}
            />
            <CardContent
              aria-label={`listing-${listing['id']}`}
              id={`i${listing['id']}`}
            >
              <Typography variant="h5" gutterBottom>
                {listing.listing.price}
              </Typography>
              <Typography variant="h6" gutterBottom>
                {listing.listing.title}
              </Typography>
              <Typography variant="subtitle1" gutterBottom>
                {listing.listing.location}
              </Typography>
              <Typography variant="subtitle1" gutterBottom>
                {listing.listing.description}
              </Typography>
              {props.loggedIn ?
                <div style={{marginTop: 10}}>
                  <p>Message the Seller: </p>
                  <input
                    aria-label='response'
                    type='text'
                    defaultValue='Type a response'
                    className={classes.input}
                    onChange={(e) => handleInputChange(e)}
                    onKeyDown={(e) => handleKeyPress(e)}
                  />
                  {getResponse()}
                </div> :
                <div> Log in to Respond </div>
              }
            </CardContent>
          </Card>
          }
          {listing === 'Failed to get listing' &&
            <div>{listing}</div>
          }
        </Drawer>
      }
    </div>
  );
}

export default ListingsPopUp;
