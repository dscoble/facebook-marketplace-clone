import React from 'react';
import {AppBar, Toolbar, Button, Typography, Grid} from '@material-ui/core';
import {useState, useEffect} from 'react';
import LoginFast from './LoginFast';

/**
 * Simple component with no state.
 * @param {object} props props
 * @return {object} JSX
 */
function TopBar(props) {
  // console.log(props.setLoggedIn);
  const [state, setState] = useState({
    width: window.innerWidth,
    // height: window.innerHeight,
  });

  const handleResize = () => {
    setState({
      width: window.innerWidth,
      // height: window.innerHeight,
    });
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize, false);
    return () => {
      setState({});
    };
  }, []);

  return (
    <AppBar style={{backgroundColor: 'white', justify: 'space-between'}}>
      <Toolbar variant="dense">
        <Grid
          justifyContent="space-between"
          container
          spacing={10}
        >
          <Grid item>
            <Typography style={{
              color: 'blue', fontWeight: 'bold',
              marginTop: 3,
            }} variant="h6"
            color="inherit" component="div">
              facebook
            </Typography>
          </Grid>
          <Grid item>
            {state.width > 600 ?
              (<LoginFast setOpenLogin={props.setOpenLogin}
                setLoggedIn = {props.setLoggedIn}></LoginFast>) :
              (<Button aria-label='log in'
                onClick={() => props.setOpenLogin(true)}
                style={{
                  backgroundColor: 'blue', color: 'white',
                  display: 'flex',
                }}
              >
                Log In
              </Button>)}
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}

export default TopBar;
