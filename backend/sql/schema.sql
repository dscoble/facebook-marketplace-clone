-- Dummy table --
DROP TABLE IF EXISTS dummy;
CREATE TABLE dummy(created TIMESTAMP WITH TIME ZONE);

-- Your database schema goes here --
DROP TABLE IF EXISTS responses;

DROP TABLE IF EXISTS listings;

DROP TABLE IF EXISTS members;

DROP TABLE IF EXISTS categories;

CREATE TABLE members(id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), member jsonb);

CREATE TABLE categories(id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), category jsonb);

ALTER TABLE categories ADD COLUMN parent UUID REFERENCES categories (id);

CREATE TABLE listings(id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), listing jsonb);

ALTER TABLE listings ADD COLUMN member UUID REFERENCES members (id);

ALTER TABLE listings ADD COLUMN category UUID REFERENCES categories (id);

CREATE TABLE responses(id UUID UNIQUE PRIMARY KEY DEFAULT gen_random_uuid(), response jsonb);

ALTER TABLE responses ADD COLUMN listing UUID REFERENCES listings (id);