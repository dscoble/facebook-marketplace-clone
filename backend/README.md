## Routes
### POST /signup 
* Adds a new user to the database
<br><br>
Input:
```
{
  "email": "user@example.com",
  "password": "string"
}
```

### POST /authenticate 
* Authenticates a user
<br><br>
Input:
```
{
  "email": "user@example.com",
  "password": "string"
}
```

### GET /user/:id
* Gets a user by ID
<br><br>
Input:
```
<id of the user>
```

### GET /listings
* Gets all listings

### GET /categories
* Gets all categories

### GET /responses/:id

* Gets all responses for a listing by Id

<br><br>
Input:
```
<id of the listing>
```
