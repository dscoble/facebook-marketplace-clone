/**
 * Credits: auth example in authenticated books
 */
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('./db');

exports.authenticate = async (req, res) => {
  const {email, password} = req.body;
  const user = await db.getUserByEmail(email);
  let correct = false;
  if (user) {
    const match = bcrypt.compareSync(password, user.member.pass);
    correct = match && user.member.email === email;
  }
  if (correct) {
    const accessToken = jwt.sign(
      {email: user.member.email},
      'token', {
        expiresIn: '30m',
        algorithm: 'HS256',
      });
    res.status(200).json({
      email: user.member.email,
      firstname: user.member.firstname,
      lastname: user.member.lastname,
      id: user.id,
      accessToken: accessToken,
    });
  } else {
    res.status(401).send('Username or password incorrect');
  }
};

exports.check = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];
  jwt.verify(token, 'token', (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }
    req.user = user;
    next();
  });
};

