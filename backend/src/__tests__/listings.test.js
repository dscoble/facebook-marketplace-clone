const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

const defaultListingSize = 200;
const listingId = '2c22fc4f-0757-4272-9206-9027f77a5e82';
const invalidListingId = '4944550c-9fec-4bb4-8dfd-1e5b411fa0b0';
const userId = 'fbec17cc-9d39-4bff-be43-afc60e5f80db';
const newListing = {
  'member': userId,
  'category': '979e8379-7d29-43ea-a4f6-e6ce053071f7',
  'listing': {
    'description': 'valid listing',
    'price': '$0',
    'title': 'new listing',
    'location': 'santa cruz, ca',
    'images': [
      'image.com',
    ],
  },
};
const credentials = {
  'email': 'user@example.com',
  'password': 'user',
};
const filter = {
  'category': 'c7c385c1-d889-40e4-bd5c-a5d2806700b5',
  'filters': [
    {
      'name': 'color',
      'value': 'Red',
    },
  ],
};
// const subcategoryId = '995933b9-644a-4fe1-b6bb-eedc9c94976c';
let user;
let server;

beforeAll(async () => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  await request.post('/v0/authenticate')
    .send(credentials)
    .then((data) => {
      user = data.body;
    });
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});

test('GET Invalid URL', async () => {
  await request.get('/v0/so-not-a-real-end-point-ba-bip-de-doo-da/')
    .expect(404);
});

test('GET ALL listings', async () => {
  await request.get('/v0/listings')
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
      expect(data.body.length).toBe(defaultListingSize);
    });
});

test('GET listing by ID', async () => {
  await request.get(`/v0/listings/${listingId}`)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
    });
});

test('GET listing with invalid ID', async () => {
  await request.get(`/v0/listings/${invalidListingId}`)
    .expect(404);
});

test('SEARCH listings with substring', async () => {
  await request.get(`/v0/search?substring=a`)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
    });
});

test('POST new valid listing with no auth', async () => {
  await request.post(`/v0/listings`)
    .send(newListing)
    .expect(401);
});

test('POST new valid listing with auth', async () => {
  await request.post(`/v0/listings`)
    .set('Authorization', `Bearer ${user.accessToken}`)
    .send(newListing)
    .expect(201)
    .then((data) => {
      expect(data.body).toBeDefined();
      expect(data.body.member).toBe(newListing.member);
      expect(data.body.category).toBe(newListing.category);
      expect(data.body.listing.description)
        .toBe(newListing.listing.description);
      expect(data.body.listing.price)
        .toBe(newListing.listing.price);
      expect(data.body.listing.title)
        .toBe(newListing.listing.title);
      expect(data.body.listing.location)
        .toBe(newListing.listing.location);
      expect(data.body.listing.images).toBeDefined();
    });
});

test('GET listings for user by userId with no auth', async () => {
  await request.get(`/v0/listings/user/${userId}`)
    .expect(401);
});

test('GET listings for user by userId with auth', async () => {
  await request.get(`/v0/listings/user/${userId}`)
    .set('Authorization', `Bearer ${user.accessToken}`)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
      for (let i = 0; i < data.body.length; i++) {
        expect(data.body[i].member).toBe(userId);
      }
    });
});

test('GET listings with incorrect auth header', async () => {
  await request.get(`/v0/listings/user/${userId}`)
    .set('Authorization', `Bearer ${user.accessToken}123`)
    .expect(403);
});

test('GET listings with incorrect auth header', async () => {
  await request.get(`/v0/listings/user/${userId}`)
    .set('Authorization', `Bearer ${user.accessToken}123`)
    .expect(403);
});

test('GET listings with missing auth header', async () => {
  await request.get(`/v0/listings/user/${userId}`)
    .set('Authorization', null)
    .expect(401);
});

test('FILTER listing by color for vehicles', async () => {
  await request.post('/v0/filter')
    .send(filter)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
      expect(data.body[0].category).toBe(filter.category);
      const name = filter.filters[0]['name'];
      expect(data.body[0]['listing'][name])
        .toBe(filter.filters[0]['value']);
    });
});
