const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

let server;

const correctCredentials = {
  'email': 'user@example.com',
  'password': 'user',
};

const incorrectCredentials = {
  'email': 'user@example.com',
  'password': 'adifjkldf',
};

const missingEmail = {
  'email': 'missingemail@email.com',
  'password': 'adifjkldf',
};

const newAccount = {
  email: 'newemail@example.com',
  password: 'newemail',
  firstname: 'new',
  lastname: 'email',
};

const userId = 'fbec17cc-9d39-4bff-be43-afc60e5f80db';
const invalidUserId = 'fbec17cc-9d39-4bff-be43-afc60e5f80d0';

beforeAll(() => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});

test('GET Invalid URL', async () => {
  await request.get('/v0/so-not-a-real-end-point-ba-bip-de-doo-da/')
    .expect(404);
});

test('Sign in with correct credentials', async () => {
  await request.post('/v0/authenticate')
    .send(correctCredentials)
    .expect(200)
    .then((data) => {
      expect(data.body.email).toBe(correctCredentials.email);
      expect(data.body.accessToken).toBeDefined();
      expect(data.body.id).toBeDefined();
    });
});

test('Sign in with incorrect credentials', async () => {
  await request.post('/v0/authenticate')
    .send(incorrectCredentials)
    .expect(401);
});

test('Sign in with incorrect credentials', async () => {
  await request.post('/v0/authenticate')
    .send(missingEmail)
    .expect(401);
});

test('GET user by Id', async () => {
  await request.get(`/v0/user/${userId}`)
    .expect(200)
    .then((data) => {
      expect(data.body.id).toBe(userId);
      expect(data.body.email).toBe(correctCredentials.email);
      expect(data.body.firstname).toBeDefined();
      expect(data.body.lastname).toBeDefined();
    });
});

test('GET user with invalid Id', async () => {
  await request.get(`/v0/user/${invalidUserId}`)
    .expect(404);
});

test('Signup for account', async () => {
  await request.post('/v0/signup')
    .send(newAccount)
    .expect(201);
});

test('Sign in with new credentials', async () => {
  await request.post('/v0/authenticate')
    .send({
      email: newAccount.email,
      password: newAccount.password,
    })
    .expect(200)
    .then((data) => {
      expect(data.body.email).toBe(newAccount.email);
      expect(data.body.accessToken).toBeDefined();
      expect(data.body.id).toBeDefined();
    });
});
