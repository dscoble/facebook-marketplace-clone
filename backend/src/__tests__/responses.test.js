const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

const listingId = '2c22fc4f-0757-4272-9206-9027f77a5e82';
const response = {
  'listing': listingId,
  'response': {
    'firstname': 'User',
    'lastname': 'Name',
    'text': 'Love this!',
  },
};

const invalidResponse = {
  'response': {
    'firstname': 'User',
    'lastname': 'Name',
    'text': 'Love this!',
  },
};

const credentials = {
  'email': 'user@example.com',
  'password': 'user',
};

let user;

let server;

beforeAll(async () => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  await request.post('/v0/authenticate')
    .send(credentials)
    .then((data) => {
      user = data.body;
    });
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});

test('GET Invalid URL', async () => {
  await request.get('/v0/so-not-a-real-end-point-ba-bip-de-doo-da/')
    .expect(404);
});

test('GET all responses for listing by id (no auth)', async () => {
  await request.get(`/v0/responses/${listingId}`)
    .expect(401);
});

test('INSERT new response for listing by id (no auth)', async () => {
  await request.post(`/v0/responses`)
    .send(response)
    .expect(401);
});

test('INSERT new invalid response for listing by id (no auth)', async () => {
  await request.post(`/v0/responses`)
    .send(invalidResponse)
    .expect(401);
});

test('GET all responses for listing by id (with auth)', async () => {
  await request.get(`/v0/responses/${listingId}`)
    .set('Authorization', `Bearer ${user.accessToken}`)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
    });
});

test('INSERT new response for listing by id (with auth)', async () => {
  await request.post(`/v0/responses`)
    .set('Authorization', `Bearer ${user.accessToken}`)
    .send(response)
    .expect(201)
    .then((data) => {
      expect(data.body).toBeDefined();
      expect(data.body.listing).toBe(response.listing);
      expect(data.body.response.firstname).toBe(response.response.firstname);
      expect(data.body.response.lastname).toBe(response.response.lastname);
      expect(data.body.response.text).toBe(response.response.text);
    });
});

test('INSERT new invalid response for listing by id (with auth)', async () => {
  await request.post(`/v0/responses`)
    .set('Authorization', `Bearer ${user.accessToken}`)
    .send(invalidResponse)
    .expect(400);
});
