const supertest = require('supertest');
const http = require('http');

const db = require('./db');
const app = require('../app');

const categoryID = 'c7c385c1-d889-40e4-bd5c-a5d2806700b5';
const subcategoryID = '995933b9-644a-4fe1-b6bb-eedc9c94976c';

let server;

beforeAll(() => {
  server = http.createServer(app);
  server.listen();
  request = supertest(server);
  return db.reset();
});

afterAll((done) => {
  server.close(done);
});

test('GET Invalid URL', async () => {
  await request.get('/v0/so-not-a-real-end-point-ba-bip-de-doo-da/')
    .expect(404);
});

test('GET ALL categories', async () => {
  await request.get('/v0/categories')
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
      expect(data.body.length).toBeDefined();
      for (let i = 0; i < data.body.length; i++) {
        expect(data.body[i].parent).toBe(null);
      }
    });
});

test('GET subcategories for a category', async () => {
  await request.get(`/v0/categories?subcategories=${categoryID}`)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
      expect(data.body.length).toBeDefined();
    });
});

test('GET ALL listings in a category', async () => {
  await request.get(`/v0/categories/${categoryID}`)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
    });
});

test('GET ALL listings in a subcategory', async () => {
  await request.get(`/v0/categories/${subcategoryID}`)
    .expect(200)
    .then((data) => {
      expect(data.body).toBeDefined();
    });
});

test('Get listings in a subcategory', async () => {
  await request.get(`/v0/listings?subcategory=${subcategoryID}`)
    .expect(200);
});
