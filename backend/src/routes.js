const bcrypt = require('bcrypt');
const db = require('./db');

const salt = 10;

exports.signup = async (req, res) => {
  const hashedpassword = await bcrypt.hash(req.body.password, salt);
  const user = await db.insertUser(
    req.body.email, hashedpassword, req.body.firstname, req.body.lastname);
  res.status(201).json(user);
};

exports.getAllListings = async (req, res) => {
  const listings = await db.getAllListings(req.query.subcategory);
  res.status(200).json(listings);
};

exports.postNewListing = async (req, res) => {
  const listing = await db.insertListing(
    req.body.member, req.body.category, req.body.listing);
  res.status(201).json(listing);
};

exports.getAllCategories = async (req, res) => {
  const categories = await db.getAllCategories(req.query.subcategories);
  res.status(200).json(categories);
};

exports.getUserById = async (req, res) => {
  const user = await db.getUserById(req.params.id);
  if (user == undefined) {
    res.status(404).send();
  }
  res.status(200).json(user);
};

exports.getResponsesFromListing = async (req, res) => {
  const responses = await db.getResponsesForListing(req.params.id);
  res.status(200).json(responses);
};

exports.getListingById = async (req, res) => {
  const listing = await db.getListingById(req.params.id);
  if (listing == undefined) {
    res.status(404).send();
  }
  res.status(200).json(listing);
};

exports.search = async (req, res) => {
  const listings = await db.search(req.query.substring);
  res.status(200).send(listings);
};

exports.getListingsInCategory = async (req, res) => {
  const listings = await db.getListingsInCategory(req.params.id);
  res.status(200).send(listings);
};

exports.insertNewResponse = async (req, res) => {
  const response =
    await db.insertNewResponse(req.body.listing, req.body.response);
  res.status(201).send(response);
};

exports.getListingsForUser = async (req, res) => {
  const listings = await db.getListingsForUser(req.params.id);
  res.status(200).send(listings);
};

exports.filter = async (req, res) => {
  const filteredListings = await db.filter(req.body.category, req.body.filters);
  res.status(200).send(filteredListings);
};
