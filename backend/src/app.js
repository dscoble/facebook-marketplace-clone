/**
 * Credits
 * Authenticated Books example
 * https://swagger.io/docs/specification/authentication/
 * https://stackoverflow.com/questions/65023886/how-to-get-value-from-json-with-dynamic-key-in-postgres
 * https://tableplus.com/blog/2018/08/postgresql-how-to-add-a-foreign-key.html
 */

const express = require('express');
const cors = require('cors');
const yaml = require('js-yaml');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const path = require('path');
const OpenApiValidator = require('express-openapi-validator');

const dummy = require('./dummy');
const auth = require('./auth');
const routes = require('./routes');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const apiSpec = path.join(__dirname, '../api/openapi.yaml');

const apidoc = yaml.load(fs.readFileSync(apiSpec, 'utf8'));
app.use('/v0/api-docs', swaggerUi.serve, swaggerUi.setup(apidoc));

app.use(
  OpenApiValidator.middleware({
    apiSpec: apiSpec,
    validateRequests: true,
    validateResponses: true,
  }),
);

app.get('/v0/dummy', dummy.get);
// Your routes go here
app.post('/v0/authenticate', auth.authenticate);
app.post('/v0/signup', routes.signup);
app.get('/v0/listings', routes.getAllListings);
app.post('/v0/listings', auth.check, routes.postNewListing);
app.get('/v0/categories', routes.getAllCategories);
app.get('/v0/user/:id', routes.getUserById);
app.get('/v0/responses/:id', auth.check, routes.getResponsesFromListing);
app.get('/v0/listings/:id', routes.getListingById);
app.get('/v0/search', routes.search);
app.get('/v0/categories/:id', routes.getListingsInCategory);
app.post('/v0/responses', auth.check, routes.insertNewResponse);
app.get('/v0/listings/user/:id', auth.check, routes.getListingsForUser);
app.post('/v0/filter', routes.filter);

app.use((err, req, res, next) => {
  res.status(err.status).json({
    message: err.message,
    errors: err.errors,
    status: err.status,
  });
});

module.exports = app;
