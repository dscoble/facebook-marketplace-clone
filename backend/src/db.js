
const {Pool} = require('pg');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  database: process.env.POSTGRES_DB,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
});

exports.getUserByEmail = async (email) => {
  const select = `SELECT * FROM members WHERE member->>'email' = $1`;
  const query = {
    text: select,
    values: [email],
  };
  const {rows} = await pool.query(query);
  return rows[0];
};

exports.insertUser = async (email, pass, firstname, lastname) => {
  const insert =
    `INSERT INTO members(member) VALUES 
      ($1) RETURNING *`;
  const user = {
    email: email,
    pass: pass,
    firstname: firstname,
    lastname: lastname,
  };
  const query = {
    text: insert,
    values: [user],
  };
  const {rows} = await pool.query(query);
  const row = rows[0];
  return {
    id: row.id,
    email: row.member.email,
    lastname: row.member.lastname,
    firstname: row.member.firstname,
  };
};

exports.getAllListings = async (subcategories) => {
  let get = `SELECT * FROM listings`;
  if (subcategories) {
    get += ` WHERE listing->>'subcategory' = $1`;
  }
  const query = {
    text: get,
    values: subcategories ? [subcategories] : [],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.insertListing = async (member, category, listing) => {
  const insert = `INSERT INTO listings(listing, member, category)
    VALUES ($1, $2, $3) RETURNING *`;
  listing['date'] = new Date().toISOString();
  const query = {
    text: insert,
    values: [listing, member, category],
  };
  const {rows} = await pool.query(query);
  return rows[0];
};

exports.getAllCategories = async (category) => {
  let select = 'SELECT * FROM categories';
  if (category) {
    select += ' WHERE parent = $1';
  } else {
    select += ' WHERE parent IS NULL';
  }
  const query = {
    text: select,
    values: category ? [category] : [],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.getUserById = async (id) => {
  const select = 'SELECT id, member FROM members WHERE id = $1';
  const query = {
    text: select,
    values: [id],
  };
  const {rows} = await pool.query(query);
  if (rows.length == 1) {
    return {
      'id': rows[0].id,
      'email': rows[0].member.email,
      'firstname': rows[0].member.firstname,
      'lastname': rows[0].member.lastname,
    };
  }
  return undefined;
};

exports.getResponsesForListing = async (listingId) => {
  const select = 'SELECT * FROM responses WHERE listing = $1';
  const query = {
    text: select,
    values: [listingId],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.getListingById = async (listingId) => {
  const select = 'SELECT * FROM listings WHERE id = $1';
  const query = {
    text: select,
    values: [listingId],
  };
  const {rows} = await pool.query(query);
  const res = rows.length == 1 ? rows[0] : undefined;
  return res;
};

exports.search = async (substring) => {
  const select = `SELECT * FROM listings
    WHERE (listing->>'title' ILIKE '%' || $1 || '%')`;
  const query = {
    text: select,
    values: [substring],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.getListingsInCategory = async (categoryId) => {
  const select = 'SELECT * FROM listings WHERE category = $1';
  const query = {
    text: select,
    values: [categoryId],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.insertNewResponse = async (listing, response) => {
  const insert = `INSERT INTO responses (listing, response)
    VALUES ($1, $2) RETURNING *`;
  const query = {
    text: insert,
    values: [listing, response],
  };
  const {rows} = await pool.query(query);
  return rows[0];
};

exports.getListingsForUser = async (userid) => {
  const select = `SELECT * FROM listings WHERE member = $1`;
  const query = {
    text: select,
    values: [userid],
  };
  const {rows} = await pool.query(query);
  return rows;
};

exports.filter = async (category, filters) => {
  const vals = [];
  let select = `SELECT * FROM listings, jsonb_each(listing) objects`;
  for (let i = 0; i < filters.length; i++) {
    select += ` WHERE objects.value::text LIKE '%' || $1 || '%'`;
    vals.push(filters[i].value);
  }
  select += ` AND category = $2`;
  vals.push(category);
  const query = {
    text: select,
    values: vals,
  };
  const {rows} = await pool.query(query);
  return rows;
};
